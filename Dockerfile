FROM ubuntu:bionic

ARG ssh_prv_key
ARG ssh_pub_key
ARG ssh_passphrase
ARG kube_master_ip

# Prepare
RUN apt-get update
RUN apt-get -y install openssh-server sshpass wget

# Install Kubectl
RUN apt-get -y install curl apt-transport-https gnupg2 git
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update
RUN apt-get install -y kubectl
RUN mkdir /root/.kube

# Install Helm
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# Install KubeSeal
RUN wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.4/kubeseal-linux-amd64 -O kubeseal
RUN install -m 755 kubeseal /usr/local/bin/kubeseal

# Copy SSH access rights from Host
RUN mkdir /root/.ssh
RUN chmod 700 /root/.ssh
RUN echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    echo "$ssh_pub_key" > /root/.ssh/id_rsa.pub && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub
RUN  echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config

# Set the working directory
RUN mkdir /esm
WORKDIR /esm

# Configure kubectl to connect to the remote master kubernetes node
# Use second "RUN sshpass ..." option for of password access (and not passphrase)
RUN sshpass -p "$ssh_passphrase" scp -o StrictHostKeyChecking=no -o PreferredAuthentications=password root@"$kube_master_ip":/etc/kubernetes/admin.conf .
#RUN sshpass -P passphrase -p "$ssh_passphrase" scp -o StrictHostKeyChecking=no -o PreferredAuthentications=publickey root@"$kube_master_ip":/etc/kubernetes/admin.conf .
RUN cp ./admin.conf /root/.kube/config
RUN sed -i "s/127.0.0.1/$kube_master_ip/g" /root/.kube/config

# Get kube seal certificates and save them in the output folder
RUN mkdir output
RUN kubeseal --fetch-cert > ./output/pub-cert.pem
RUN kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml > ./output/master.key
RUN kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}') | grep token > ./output/dashboard-token.txt