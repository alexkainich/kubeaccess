# KubeAccess

A docker container set up to communicate with the ESM kubernetes cluster

# Preparation

You first need ssh access to the remote master kubernetes node.

# How to build and run

In the below commands first replace the `<your kube master ip>` with the ip of your master kubernetes node.
Also replace `<your ssh passphrase>` with your ssh passphrase. Then execute them in a machine with docker installed:

`docker build --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" --build-arg kube_master_ip=<your kube master ip> --build-arg ssh_passphrase=<your ssh passphrase> --tag esm-client:latest .`

`docker run -it esm-client:latest`

# Output folder

Inside the container, in /esm, you can find the output folder.
This contains the kubernetes dashboard token and the kubeseal secrets
